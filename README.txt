Downloading from GIT
--------------------

Find the directory where you want to have the project.
Right click and press "Open terminal here"
Type into the terminal: git clone git@bitbucket.org:sdp_group1/sdp.git
This will download everything into a sub directory (called sdp) of your current
directory


Eclipse Setup
--------------
For details of how to setup Eclipse, see pc/README.txt

Running the communication server
--------------------------------
For details of the comms server and how to run it see commsserver/README

Milestone 2
-----------
Look at pc/src/milestone2/Milestone2.java

This has a function that allows the robot to follow the ball for 20 seconds

And a function that move in a certain direction.
