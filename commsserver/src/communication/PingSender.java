package communication;

import sdp.common.RobotCommand;

public class PingSender extends Thread {
	private int mTimeout;
	private Command mCommand;
	
	public PingSender(Command cmd, int timeout){
		mTimeout = timeout;
		mCommand = cmd;
	}
	
	public void run(){
		while ( !isInterrupted() ){
			byte[] pingCommand = new byte[Constants.COMMAND_SIZE];
			pingCommand[0] = RobotCommand.PING;
			mCommand.set(pingCommand);
			try {
				Thread.sleep(mTimeout);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
