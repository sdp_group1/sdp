package communication;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Command Storage with concurrency support for CommunicationServer
 * 
 * @author Sarun Gulyanon
 * 
 */
public class Command {
	private static final int TIMEOUT = 300000;

	Queue<byte[]> mCmds = new LinkedList<byte[]>();

	public Command() {
		
	}

	public synchronized void set(byte[] command) {
		mCmds.add(command);
		notify();
	}

	public synchronized byte[] get() {
		while (mCmds.isEmpty()) {
			try {
				wait(TIMEOUT);
			} catch (InterruptedException e) {
				System.err.println("Error Getting Command");
			}
		}
		return mCmds.remove();
	}
}
