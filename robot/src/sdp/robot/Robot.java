package sdp.robot;

import sdp.common.RotationDirection;
import lejos.nxt.*;

public class Robot {
    public I2cMotor[] mMotors;

    private boolean mIsRotating;
    private RotationDirection mRotationDirection;

    private boolean mIsMoving = false;
    private double mMoveDirection = 0;
    
    private int mMaximumMovementSpeed = 255;

	private double mRotationWeight;
    
	public void init() {
		I2CPort i2Cport; 
		i2Cport = SensorPort.S4;
		i2Cport.i2cEnable(I2CPort.STANDARD_MODE); 
		I2CSensor i2Csensor = new I2CSensor(i2Cport);
		i2Csensor.setAddress(0xb4);
		
		I2cMotor[] motors = new I2cMotor[4];
		motors[0] = new I2cMotor(i2Csensor, 0x01, Math.PI/2, 1);
		motors[1] = new I2cMotor(i2Csensor, 0x03, Math.PI, 1);
		motors[2] = new I2cMotor(i2Csensor, 0x05, Math.PI/2, -1);
		motors[3] = new I2cMotor(i2Csensor, 0x07, Math.PI, -1);
		
		initWithMotors(motors);
	}
	
	public void initWithMotors(I2cMotor[] motors) {
		mMotors = motors;
	}
	
	public void close() {
		Sound.beep();
	}
	
	public void stopMoving(){
        mIsMoving = false;
        updateSpeeds();
	}

	public void stopRotating(){
        mIsRotating = false;
        updateSpeeds();
	}

    public void rotate(RotationDirection direction, double weight){
        mIsRotating = true;
        mRotationDirection = direction;
        mRotationWeight = weight;
        updateSpeeds();
    }
	
	public void move(float direction) {
        mIsMoving = true;
        mMoveDirection = direction;
        updateSpeeds();
	}
    
    private void updateSpeeds(){
		final double cosDirection = Math.cos(mMoveDirection);
		final double sinDirection = Math.sin(mMoveDirection);
		
        double[] motorSpeed = new double[mMotors.length];
        double maxSpeed = 0;
        for ( int i = 0; i < mMotors.length; ++i ){
            final I2cMotor m = mMotors[i];
			final double angle = m.getWheelAngle();

            motorSpeed[i] = 0;
            if ( mIsMoving ){
				motorSpeed[i] += Math.cos(angle) * cosDirection
	 						   + Math.sin(angle) * sinDirection;
            }
            if ( mIsRotating ){
            	final double roateDirection = mRotationDirection == RotationDirection.CLOCKWISE ? 1 : -1;
                motorSpeed[i] += mRotationWeight * roateDirection * m.getWheelDirectionModifier();
            }
            maxSpeed = Math.max(Math.abs(motorSpeed[i]), maxSpeed);
		}
        
        final double epsilon = 1.0/(I2cMotor.MAX_MOTOR_SPEED + 1);
        if ( maxSpeed > epsilon ){
	        for ( int i = 0; i < motorSpeed.length; ++i ){
	        	if ( mIsRotating && !mIsMoving ){
	        		motorSpeed[i] *= ((mMaximumMovementSpeed)/maxSpeed)*mRotationWeight;
	        	}else{
		        	motorSpeed[i] *= (mMaximumMovementSpeed)/maxSpeed;
	        	}
	        }
        }     
        for ( int i = 0; i < mMotors.length; ++i ){
            mMotors[i].setSpeed(Math.min(255, (int)Math.round(motorSpeed[i])));
        }
    }

	public void kick(){
		System.out.println("Kicking");
		Motor.A.setSpeed(6000);
		Motor.B.setSpeed(6000);
		Motor.A.rotate(-45,true);
		Motor.B.rotate(-45);
		Motor.A.rotate(45,true);
		Motor.B.rotate(45);
		Motor.A.stop();
		Motor.B.stop();

	}

	public void setMaximumMoveSpeed(float arg) {
		int maxSpeed = (int)arg;
		mMaximumMovementSpeed = Math.min(255, Math.max(maxSpeed, 0));
		
	}
}
