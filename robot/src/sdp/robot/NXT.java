package sdp.robot;

import sdp.common.*;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import lejos.nxt.comm.Bluetooth;
import lejos.nxt.Button;
import lejos.nxt.ButtonListener;
import lejos.nxt.comm.NXTConnection;

import sdp.common.RotationDirection;

public class NXT {
    private static final byte ROBOT_READY = 0; 
    
    private static Robot robot;
	private static Sensors sensors; 
	
    private static InputStream in;
    private static OutputStream out;

    /**
     * Create a bluetooth connection to PC
     */
    public static void establishConnection() {
        System.out.println("Waiting for Bluetooth connection...");
        NXTConnection connection = Bluetooth.waitForConnection();
        if ( connection == null ){
        	System.out.println("Failed to establish connection");
        	establishConnection();
        	return;
        }
        in = connection.openInputStream();
        out = connection.openOutputStream();
        System.out.println("Connection established!");
    }

    /**
     * Read a byte from PC
     * @return opcode
     */
    public static int readByte() {
        int opcode = 0;
        try {
            opcode = in.read();
        } catch (IOException e) {
            System.out.println("Couldn't read byte: " + e.toString());
            return -1;
        }
        return opcode;
    }

    /**
     * Read 5-bytes command from PC
     * @return command in array of bytes
     */
    public static byte[] readBytes(int numBytesToRead) {
        byte[] bytes = new byte[numBytesToRead];
        try {
            if(in.read(bytes) != numBytesToRead) {
                System.out.println("Wrong Format");
            }
        }
        catch (IOException e) {
            System.out.println("Couldn't read bytes: " + e.toString());
        }
        return bytes;
    }

    /**
     * Get int parameter from command
     * @param command - a 5 bytes command
     * @return
     */
    public static int getInt(byte[] command) {
        int val = 0;
        for (int i = 1; i < 5; i++) {
            val <<= 8;
            val += 0x000000FF & command[i];
        }
        return val;
    }

    /**
     * Get Opcode from command
     * @param command - a 5 bytes command
     * @return
     */
   public static int getOpcode(byte[] command) {
       return 0x000000FF & command[0];
   }

    /**
     * Send a byte to PC
     * @param b - a byte of data sending to PC
     */
    public static void sendByte(byte b) {
        try {
            out.write(b);
            out.flush();
        } catch (IOException e) {
            System.out.println("Couldn't send byte: " + e.toString());
        }
    }

    /**
     * Send bytes to PC
     * @param bytes
     */
    public static void sendBytes(byte[] bytes) {
        try {
            out.write(bytes);
            out.flush();
        } catch (IOException e) {
            System.out.println("Couldn't send bytes: " + e.toString());
        }
    }

    /**
     * Send 0 to PC indicating that robot is ready
     */
    public static void sendReadySignal() {
        sendByte(ROBOT_READY);
        robot.init();
    }

    /**
     * Translate the opcode
     * @param opcode
     * @return
     */
    public static boolean readCommand (int opcode, float arg) {
        switch (opcode) {
        case RobotCommand.PING:
        	break;
        case RobotCommand.MOVE:
        	robot.move(arg);
            System.out.println("moving!");
            break;
        case RobotCommand.STOP_MOVE:
        	robot.stopMoving();
        	break;
        case RobotCommand.STOP_ROTATE:
        	robot.stopRotating();
        	break;
        case RobotCommand.ROTATE_CW:
        	robot.rotate(RotationDirection.CLOCKWISE, arg);
        	break;
        case RobotCommand.ROTATE_CCW:
        	robot.rotate(RotationDirection.COUNTERCLOCKWISE, arg);
        	break;
        case RobotCommand.KICK:
        	robot.kick();
        	System.out.println("kick!");
        	break;
        case RobotCommand.MAX_MOVE_SPEED:
        	robot.setMaximumMoveSpeed(arg);
        	break;
        case RobotCommand.EXIT:
            System.out.println("exit!");
            robot.close();
            return false;
        default:
            System.out.println("UNKNOWN CMD " + opcode);
            break;
        }
        return true;
    }
    
    /**
     * Close connection to PC
     */
    public static void closeNXT() {
    	robot.close();
        Button.waitForAnyPress();
    }
    
    public static void kickProgram(){
    	robot.kick();
        Button.waitForAnyPress();
    }
    
    public static void move5Program(){
    	
        try {
        	for ( float i = 0; i < Math.PI*2; i+=Math.PI/8){
        		robot.move(i);
    			Thread.sleep(100);
        	}
        	robot.stopMoving();
        } catch (InterruptedException e) {
			e.printStackTrace();
		}

        Button.waitForAnyPress();
    }
     
    public static void main(String args[]) {
   	
        Button.ESCAPE.addButtonListener(new ButtonListener() {
			@Override
            public void buttonPressed(Button b){}

			@Override
			public void buttonReleased(Button arg0) {
				robot.stopMoving();
				robot.stopRotating();
				System.exit(0);
			}
          });

    	robot = new Robot();
     	
        establishConnection();
        sendReadySignal();
        
       	sensors = new Sensors(robot);

        while (true) {
            int opcode;
            opcode = readByte();
            if ( opcode == -1 ){
                System.out.println("No connection....");
                establishConnection();
                sendReadySignal();
                continue;
            }
            //take DataInputStream outside if ?
            if (!sensors.isActive()) {  //check the sensor thread for activity
            	DataInputStream dis = new DataInputStream(new ByteArrayInputStream(readBytes(4)));
            	float arg = 0;
            	try {
            		arg = dis.readFloat();
            	} catch (IOException e) {
            		System.out.println("failed to read arg from byte array");
            	}
            	System.out.println(opcode + "(" + arg + ")");
            	if (!readCommand(opcode, arg))
            		break;
            }
        }
        closeNXT();
    }
}
