package sdp.robot.test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;

import sdp.robot.I2cMotor;
import sdp.robot.Robot;

public class RobotTests {
	

	I2cMotor[] mMotors = new I2cMotor[4];	
	Robot mUnderTest;
	
	@Before
	public void setUp(){
		for ( int i = 0; i < 4; ++i ){
			mMotors[i] = mock(I2cMotor.class);
		}
		when(mMotors[0].getWheelAngle()).thenReturn(Math.PI/2);
		when(mMotors[1].getWheelAngle()).thenReturn(Math.PI);
		when(mMotors[2].getWheelAngle()).thenReturn(Math.PI/2);
		when(mMotors[3].getWheelAngle()).thenReturn(Math.PI);
		
		mUnderTest = new Robot();
		mUnderTest.initWithMotors(mMotors);
	}
	
	@Test
	public void move_WhenMovesForward_SetsSpeed(){
		mUnderTest.move(0);
		verify(mMotors[1]).setSpeed(-255);
		verify(mMotors[3]).setSpeed(-255);
	}
	
	@Test
	public void move_WhenMovesBackwards_SetsSpeed(){
		mUnderTest.move((float) (Math.PI));
		verify(mMotors[1]).setSpeed(255);
		verify(mMotors[3]).setSpeed(255);
	}
	
	@Test
	public void move_WhenMovesForwardAtAngle_SetsSpeed(){
		mUnderTest.move((float) (Math.PI/4));
		verify(mMotors[0]).setSpeed(255);
		verify(mMotors[1]).setSpeed(-255);
		verify(mMotors[2]).setSpeed(255);
		verify(mMotors[3]).setSpeed(-255);
	}
	

}
